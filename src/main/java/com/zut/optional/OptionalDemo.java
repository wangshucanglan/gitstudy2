package com.zut.optional;

import java.util.Optional;

/**
 * @author zpk
 * @create 2023-08-03 19:06
 */
public class OptionalDemo
{
    public static void main(String[] args) {
        Integer value1 = null;
        Integer value2 = new Integer(10);
        Optional<Integer> op1 = Optional.ofNullable(value1);//把空值放进optional容器中
        Optional<Integer> op2 = Optional.of(value2);//把非空值放进optional容器中
        OptionalDemo d1 = new OptionalDemo();
        Integer sum = d1.sum(op1, op2);
        System.out.println("sum="+sum);
    }

    public Integer sum(Optional<Integer> op1, Optional<Integer> op2) {
        System.out.println("第一个值是否存在" + op1.isPresent());
        System.out.println("第二个值是否存在" + op2.isPresent());
        Integer In1 = op1.orElse(new Integer(0));//存在返回它，否则返回默认值
        Integer In2 = op2.get();

        return In1+In2;
    }
}
