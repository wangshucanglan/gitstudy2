package com.zut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitstudeyApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitstudeyApplication.class, args);
    }

}
